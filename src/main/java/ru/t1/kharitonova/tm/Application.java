package ru.t1.kharitonova.tm;

import static ru.t1.kharitonova.tm.constant.TerminalConst.*;

public class Application {

    public static void main(String[] args) {
        run(args);
    }

    private static void run(final String[] arg){
        showWelcome();
        if(arg == null || arg.length == 0) {
            showError();
            return;
        }
       runParam(arg[0]);
    }

    private static void runParam(final String param){
        switch (param) {
            case ABOUT:
                showAbout();
                break;
            case VERSION:
                showVersion();
                break;
            case HELP:
                showHelp();
                break;
            default:
                showError();
        }
    }

    private static void showWelcome(){
        System.out.println("** WELCOME TO TASK MANAGER**");
    }

    private static void showAbout(){
        System.out.println("[ABOUT]");
        System.out.println("name: Anastasiya Kharitonova");
        System.out.println("e-mail: akharitonova@t1-consulting.ru");
    }

    private static void showVersion(){
        System.out.println("[VERSION]");
        System.out.println("1.5.0");
    }

    private static void showHelp(){
        System.out.println("[HELP]");
        System.out.printf("%s - Display developer info. \n", ABOUT);
        System.out.printf("%s - Display program version. \n", VERSION);
        System.out.printf("%s - Display list of terminal commands. \n", HELP);
    }

    private static void showError(){
        System.out.println("[ERROR]");
        System.out.println("This argument not supported");
    }

}
